package id.co.knt.airport.web;

import id.co.knt.airport.entity.TicketClass;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

@Named("ticketClassBean")
@Stateful
@SessionScoped
public class TicketClassBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private TicketClass selectedTicketClass;
	
	@PostConstruct
	public void init(){
		reset();
	}
	
	public void reset(){
		selectedTicketClass = new TicketClass();
	}
	
	public void create() {
		reset();
	}
	
	public String delete(){
		em.remove(selectedTicketClass);
		reset();
		return null;
	}
	
	public String persist(){
		em.merge(selectedTicketClass);
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("ticketClassDialog.hide()");
		
		return null;
	}
	
	public void onClose(CloseEvent e){
	}

	public TicketClass getSelectedTicketClass() {
		return selectedTicketClass;
	}

	public void setSelectedTicketClass(TicketClass selectedTicketClass) {
		this.selectedTicketClass = em.merge(selectedTicketClass);
	}
}
