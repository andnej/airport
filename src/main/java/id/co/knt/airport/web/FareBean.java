package id.co.knt.airport.web;

import id.co.knt.airport.entity.Airport;
import id.co.knt.airport.entity.Fare;
import id.co.knt.airport.entity.FareType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("fareBean")
@Stateful
@SessionScoped
public class FareBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	private static final Logger log = LoggerFactory.getLogger(FareBean.class);

	private List<Fare> fares = new ArrayList<Fare>();
	private List<Airport> airports = new ArrayList<Airport>();
	private List<FareType> fareTypes = new ArrayList<FareType>();
	private Fare selectedFare;
	private boolean dataVisible;
	private Airport searchDeparture;
	private Airport searchArrival;

	@PostConstruct
	public void init() {
		findAll();
		airports = em.createNamedQuery("Airport.findAll", Airport.class)
				.getResultList();
		fareTypes = em.createNamedQuery("fareType.findAll", FareType.class)
				.getResultList();
		reset();
	}

	public String findAll() {
		fares = em.createNamedQuery("Fare.findAll", Fare.class).getResultList();
		log.info("fares : "+ fares.size());
		dataVisible = !fares.isEmpty();

		return null;
	}

	public void reset() {
		selectedFare = new Fare();
	}

	public String delete() {
		em.remove(selectedFare);

		reset();
		return findAll();
	}

	public String persist() {
		em.merge(selectedFare);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("fareDialog.hide()");

		reset();
		return findAll();
	}

	public String filter() {
		log.info("Filter Called");
		if (searchArrival == null && searchDeparture == null) {
			fares = em.createNamedQuery("Fare.findAll", Fare.class)
					.getResultList();
		} else if (searchArrival == null && searchDeparture != null) {
			fares = em.createNamedQuery("Fare.findByDeparture", Fare.class)
					.setParameter("departure", searchDeparture).getResultList();
		} else if (searchArrival != null && searchDeparture == null) {
			fares = em.createNamedQuery("Fare.findByArrival", Fare.class)
					.setParameter("arrival", searchArrival).getResultList();
		} else if (searchArrival != null && searchDeparture != null) {
			if (searchArrival.equals(searchDeparture)) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"Departure and Arrival can not same"));
			} else {
				fares = em
						.createNamedQuery("Fare.findByArrivalAndDeparture",
								Fare.class)
						.setParameter("arrival", searchArrival)
						.setParameter("departure", searchDeparture)
						.getResultList();

			}
		}
		return null;
	}

	
	public void addAddNew(){
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("fareDialog.show()");
	}

	public void onClose(CloseEvent e) {
		reset();
	}

	public List<Fare> getFares() {
		return fares;
	}

	public void setFares(List<Fare> fares) {
		this.fares = fares;
	}

	public Fare getSelectedFare() {
		return selectedFare;
	}

	public void setSelectedFare(Fare selectedFare) {
		this.selectedFare = selectedFare;
	}

	public boolean isDataVisible() {
		return dataVisible;
	}

	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}

	public List<Airport> getAirports() {
		return airports;
	}

	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}

	public List<FareType> getFareTypes() {
		return fareTypes;
	}

	public void setFareTypes(List<FareType> fareTypes) {
		this.fareTypes = fareTypes;
	}

	public Airport getSearchDeparture() {
		return searchDeparture;
	}

	public void setSearchDeparture(Airport searchDeparture) {
		this.searchDeparture = searchDeparture;
	}

	public Airport getSearchArrival() {
		return searchArrival;
	}

	public void setSearchArrival(Airport searchArrival) {
		this.searchArrival = searchArrival;
	}

}
