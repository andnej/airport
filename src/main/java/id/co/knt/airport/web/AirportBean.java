package id.co.knt.airport.web;

import id.co.knt.airport.entity.Airport;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("airportBean")
@Stateful
@SessionScoped
public class AirportBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger log = LoggerFactory.getLogger(AirportBean.class);

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Inject
	private Event<Airport> airportEvent;
		
	private Airport selectedAirport;
	
	@PostConstruct
	public void init(){
		reset();
	}
	
	public void reset(){
		if(selectedAirport != null)
			log.debug("airport before reset : " + selectedAirport.toString());
		selectedAirport = new Airport();
		if(selectedAirport != null)
			log.debug("airport after reset : " + selectedAirport.toString());
	}
	
	public void create() {
		reset();
	}
	
	public String delete(){
		em.remove(selectedAirport);
		
		return null;
	}
	
	public String persist(){
		airportEvent.fire(em.merge(selectedAirport));
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("airportDialog.hide()");
		
		return null;
	}
	
	public void onClose(CloseEvent e){
	}
	
	public Airport getSelectedAirport() {
		return selectedAirport;
	}
	
	public void setSelectedAirport(Airport selectedAirport) {
		this.selectedAirport = em.merge(selectedAirport);
	}
	
	@Remove
	public void destroy() {}
}
