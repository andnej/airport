package id.co.knt.airport.web;

import id.co.knt.airport.entity.Booking;
import id.co.knt.airport.entity.Passenger;
import id.co.knt.airport.entity.Payment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.event.CloseEvent;

@Named("paymentBean")
@Stateful
@SessionScoped
public class PaymentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private List<Payment> payments = new ArrayList<Payment>();
	private List<Booking> bookings = new ArrayList<Booking>();
	private List<Booking> selectedBookings;
	private List<Passenger> passengers = new ArrayList<Passenger>();
	private Payment selectedPayment;
	private Booking searchBookingCode;
	private Passenger searchPassenger;
	private String firstName;
	private String lastName;
	private boolean dataVisible;
	
	@PostConstruct
	public void init() {
		findAll();
		reset();
	}
	
	public String findAll() {
		payments.clear();
		payments.addAll(em.createNamedQuery("Payment.findAll", Payment.class).getResultList());
		dataVisible = !payments.isEmpty();
		return null;
	}
	
	public void reset() {
		selectedPayment = new Payment();
		searchPassenger = new Passenger();		
	}
	
	public String delete() {
		em.remove(selectedPayment);
		return findAll();
	}
	
	public void onBookingUpdate(@Observes Booking updated) {
		boolean update = false;
		for (Booking booking : bookings) {
			if (booking.equals(updated)) {
				update = true;
				booking.setCode(updated.getCode());
				booking.setPassengers(updated.getPassengers());
				break;
			}
		}
		if (!update) {
			bookings.add(updated);
		}
	}
	
	public String persist() {
		em.merge(selectedPayment);
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Saved!"));
		return findAll();
	}
	
	public String findBookingByPassenger(){
		if(firstName != null && lastName != null){
			searchPassenger = em.createNamedQuery("Passenger.findByFirstNameAndLastName", Passenger.class)
					.setParameter("firstName", firstName)
					.setParameter("lastName", lastName)
					.getSingleResult();
		}
		else if(firstName == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("fill first name"));
		}
		else if(lastName == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("fill last name"));
		}
		
		
		
		return null;
	}
	
//	public String filter() {
//		if (searchBookingCode == null && searchPassenger == null) {
//			payments = em.createNamedQuery("payment.findAll", Payment.class)
//					.getResultList();
//		} else if (searchBookingCode == null && searchPassenger != null) {
//			payments = em.createNamedQuery("payment.findByBookingCode", Payment.class)
//					.setParameter("bookingCode", searchBookingCode).getResultList();
//		} else if (searchBookingCode != null && searchPassenger == null) {
//			payments = em.createNamedQuery("payment.findByPassenger", Payment.class)
//					.setParameter("passenger", searchPassenger).getResultList();
//		} else if (searchBookingCode != null && searchPassenger != null) {
//			if (searchBookingCode.equals(searchPassenger)) {
//				FacesContext context = FacesContext.getCurrentInstance();
//				context.addMessage(null, new FacesMessage(
//						"Choice one"));
//			} else {
//				payments = em
//						.createNamedQuery("payment.findByBookingCodeAndPassenger",
//								Payment.class)
//						.setParameter("bookingCode", searchBookingCode)
//						.setParameter("passenger", searchPassenger)
//						.getResultList();
//
//			}
//		}
//		return null;
//	}
	
	public void onClose(CloseEvent e) {
		reset();
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public List<Booking> getSelectedBookings() {
		return selectedBookings;
	}

	public void setSelectedBookings(List<Booking> selectedBookings) {
		this.selectedBookings = selectedBookings;
	}

	public Payment getSelectedPayment() {
		return selectedPayment;
	}

	public void setSelectedPayment(Payment selectedPayment) {
		this.selectedPayment = selectedPayment;
	}

	public boolean isDataVisible() {
		return dataVisible;
	}

	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public Booking getSearchBookingCode() {
		return searchBookingCode;
	}

	public void setSearchBookingCode(Booking searchBookingCode) {
		this.searchBookingCode = searchBookingCode;
	}

	public Passenger getSearchPassenger() {
		return searchPassenger;
	}

	public void setSearchPassenger(Passenger searchPassenger) {
		this.searchPassenger = searchPassenger;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
