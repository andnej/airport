package id.co.knt.airport.web;

import id.co.knt.airport.entity.Booking;
import id.co.knt.airport.entity.Fare;
import id.co.knt.airport.entity.FlightSchedule;
import id.co.knt.airport.entity.Leg;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("legBean")
@Stateful
@SessionScoped
public class LegBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private static Logger log = LoggerFactory.getLogger(LegBean.class);
	
	private List<FlightSchedule> flightSchedules;
	private FlightSchedule selectedFlightSchedule;
	private List<Leg> legs;
	private Leg newLeg;
	private List<Booking> bookings = new ArrayList<Booking>();
	private List<Fare> fares;
	
	@Temporal(TemporalType.DATE)
	private Date todayDate = new Date();
	@Temporal(TemporalType.DATE)
	private Date fromDate;
	@Temporal(TemporalType.DATE)
	private Date toDate;
	private boolean dataVisible = true;
	
	@PostConstruct
	public void init(){
		fromDate = new Date();
		toDate = new Date();
		findByBetweenDate();
		reset();
	}
	
	private String findByBetweenDate(){
		System.out.println("call findByBetweenDate");
		flightSchedules = em.createNamedQuery("FlightSchedule.findByBetweenDate", FlightSchedule.class)
				.setParameter("fromDate", fromDate)
				.setParameter("toDate", toDate)
				.getResultList();
		
		return null;
	}
	
	public void reset(){
		newLeg = new Leg();
	}
	
	public String getSchedule(){
		legs = selectedFlightSchedule.getLegs();
		getBookingList();
		dataVisible = !(selectedFlightSchedule == null);
		
		return null;
	}
	
	public String filterSchedule(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);
		String today = sdf.format(todayDate);
	
		if(fromDate != null && toDate != null){
			if(from.equals(today)){
				if(to.equals(today)){
					findByBetweenDate();
					log.info("from == to");
				}
				else if(toDate.after(todayDate)){
					findByBetweenDate();
					log.info("from < to");
				}
				else if(toDate.before(todayDate)){
					expiredArrival();
					log.info("from > to");
				}				
			}
			else if(fromDate.after(todayDate)){
				if(to.equals(today)){
					arrivalMessage();
				}
				if(toDate.after(todayDate)){
					findByBetweenDate();
					log.info("from and to > today");
				}
				else if(toDate.before(todayDate)){
					expiredArrival();
					log.info("from > to");
				}
			}			
			else if(fromDate.before(todayDate)){
				expiredDeparture();
				log.info("from < today");
				if(toDate.before(todayDate)){
					expiredArrival();
					log.info("to < today");
				}
			}
			log.info("filterSchedule called");
		}
		return null;
	}
			
	private String arrivalMessage(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your arrival date can not before departure date"));
		
		return null;
	}
	
	private String expiredDeparture(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your departure date is expired"));
		
		return null;
	}
	
	private String expiredArrival(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your arrival date is expired"));
		
		return null;
	}
	
	public String persist(){
		selectedFlightSchedule.addLeg(newLeg);
		em.merge(newLeg);
		getSchedule();
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("legDialog.hide()");
		
		reset();
		return null;
	}
	
	public String getBookingList(){
		fares = selectedFlightSchedule.getFares();
		for (Fare fare : fares) {
			bookings.addAll(fare.getBookings());
			break;
		}
		log.info("booking size = " + bookings.size());
		
		return null;
	}
	
	public void onClose(CloseEvent e){
		reset();
	}
	
	public List<FlightSchedule> getFlightSchedules() {
		return flightSchedules;
	}
	
	public void setFlightSchedules(List<FlightSchedule> flightSchedules) {
		this.flightSchedules = flightSchedules;
	}
	
	public List<Leg> getLegs() {
		return legs;
	}

	public void setLegs(List<Leg> legs) {
		this.legs = legs;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public Leg getNewLeg() {
		return newLeg;
	}
	
	public void setNewLeg(Leg newLeg) {
		this.newLeg = newLeg;
	}
	
	public FlightSchedule getSelectedFlightSchedule() {
		return selectedFlightSchedule;
	}

	public void setSelectedFlightSchedule(FlightSchedule selectedFlightSchedule) {
		this.selectedFlightSchedule = selectedFlightSchedule;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	public boolean isDataVisible() {
		return dataVisible;
	}
	
	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}
	
}
