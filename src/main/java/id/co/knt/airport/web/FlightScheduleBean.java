package id.co.knt.airport.web;

import id.co.knt.airport.entity.Airport;
import id.co.knt.airport.entity.Fare;
import id.co.knt.airport.entity.FlightSchedule;
import id.co.knt.airport.qualifier.Arrival;
import id.co.knt.airport.qualifier.Departure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("flightScheduleBean")
@Stateful
@SessionScoped
public class FlightScheduleBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private static Logger log = LoggerFactory.getLogger(FlightScheduleBean.class);
	private FlightSchedule selectedFlightSchedule;
	private Airport departure;
	private Airport arrival;
	private boolean dataVisible = false;
	private List<Fare> fares = new ArrayList<Fare>();
	private Fare selectedFare;
	
	@PostConstruct
	public void init(){
		reset();
	}
	
	public String findAllFare(){
		fares = em.createQuery("select f from Fare f where f.flightSchedule = :fl",Fare.class)
				.setParameter("fl", selectedFlightSchedule)
				.getResultList();
		return null;
	}
	
	public void reset(){
		selectedFlightSchedule = new FlightSchedule();
	}

	public void resetFare(){
		selectedFare = new Fare();
	}
	
	public String airportCheck(){
		if(departure != null && arrival != null){
			if(departure.equals(arrival)){
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage("departure and arrival can not same"));
			}
			else {
				selectedFlightSchedule.setDepartureAirport(departure);
				selectedFlightSchedule.setArrivalAirport(arrival);
				createNew();
			}
		}
		else {
			if(departure != null)
				selectedFlightSchedule.setDepartureAirport(departure);
			if(arrival != null)
				selectedFlightSchedule.setArrivalAirport(arrival);
			createNew();
		}
		
		return null;
	}
	
	public void createNew(){
		reset();
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("flightScheduleDialog.show()");
		log.info("createNew called");
	}
	
	public String delete(){
		em.remove(selectedFlightSchedule);		
		reset();
		return null;
	}
	
	public String deleteFare(){
		em.remove(selectedFare);		
		resetFare();
		return findAllFare();
	}

	
	public String persist(){
		em.merge(selectedFlightSchedule);
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("flightScheduleDialog.hide()");
		
		reset();
		return null;
	}
	
	public String showFare(FlightSchedule fs){
		this.selectedFlightSchedule=fs;
		selectedFare = new Fare();
		findAllFare();
		return "fare";
	}
	
	public String backToFlightSchedule(){
		reset();
		return "flight_schedule";
	}

	public String persistFare(){
		selectedFare.setFlightSchedule(selectedFlightSchedule);
//		selectedFlightSchedule.getFares().add(selectedFare);
		em.merge(selectedFare);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		findAllFare();
		System.out.println("flight number 2: "+selectedFlightSchedule.getFlightNumber());		
		request.execute("fareDialog.hide()");
		return null;
	}

	
	public void onClose(CloseEvent e){
		reset();
	}

	public void onCloseFare(CloseEvent e){
		resetFare();
	}
	
	public FlightSchedule getSelectedFlightSchedule() {
		return selectedFlightSchedule;
	}

	public void setSelectedFlightSchedule(FlightSchedule selectedFlightSchedule) {
		this.selectedFlightSchedule = selectedFlightSchedule;
		fares = selectedFlightSchedule.getFares();
	}

	@Produces
	@Departure
	public Airport getDeparture() {
		return departure;
	}

	public void setDeparture(Airport departure) {
		this.departure = departure;
	}

	@Produces
	@Arrival
	public Airport getArrival() {
		return arrival;
	}

	public void setArrival(Airport arrival) {
		this.arrival = arrival;
	}

	public boolean isDataVisible() {
		return dataVisible;
	}
	
	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}

	public List<Fare> getFares() {
		return fares;
	}

	public void setFares(List<Fare> fares) {
		this.fares = fares;
	}

	public Fare getSelectedFare() {
		return selectedFare;
	}

	public void setSelectedFare(Fare selectedFare) {
		this.selectedFare = selectedFare;
	}
	
}
