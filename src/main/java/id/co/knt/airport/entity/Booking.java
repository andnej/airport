package id.co.knt.airport.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Entity implementation class for Entity: Booking
 *
 */
@Entity
@Table(name="BOOKINGS")
@NamedQueries({
	@NamedQuery(name="Booking.findAll", query="select b from Booking b")
})
public class Booking implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Booking() {
		super();
	}
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="BOOKING_DATE")
	@Temporal(TemporalType.DATE)
	private Date date;
	@Column(name="CODE", unique=true)
	private String code;
	@ManyToMany(mappedBy="bookings", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Passenger> passengers = new ArrayList<Passenger>();
	@Column(name="BOOKING_TYPE")
	private Boolean bookingType;
	@Transient
	private BigDecimal total;
	
	@ManyToOne
	@JoinColumn(name="FARE_ID")
	private Fare fare;
	@ManyToOne
	@JoinColumn(name = "PAYMNET_ID")
	private Payment payment;

	@Override
	public String toString() {
		return "Booking [id=" + id + ", date=" + date + ", passenger="
				 + ", fare=" + fare + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((fare == null) ? 0 : fare.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booking other = (Booking) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (fare == null) {
			if (other.fare != null)
				return false;
		} else if (!fare.equals(other.fare))
			return false;
		return true;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Fare getFare() {
		return fare;
	}

	public void setFare(Fare fare) {
		this.fare = fare;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Long getId() {
		return id;
	}
	
	public void calculateTotal(){
		total = BigDecimal.ZERO;
		for (Passenger passenger : passengers) {
			if(bookingType){
				total= total.add(this.fare.getOneWayFare());
			}else{
				total= total.add(this.fare.getReturnFare());
			}
		}

	}
	
	
	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}
	
	

	public Boolean getBookingType() {
		return bookingType;
	}

	public void setBookingType(Boolean bookingType) {
		this.bookingType = bookingType;
	}
	
	

	public BigDecimal getTotal() {
		return total;
	}

	public void addPayment(Payment p) {
		if (p != null) {
			
		}
	}
}
