package id.co.knt.airport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="FARE_TYPES")
@NamedQueries({
	@NamedQuery(name="FareType.findAll", query="select ft from FareType ft")
})
public class FareType implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID")
	private Long id;
	@ManyToOne
	@JoinColumn(name="TICKET_CLASS_ID")
	private TicketClass ticketClass;
	@Column(name="DESCRIPTION")
	private String description;
	
	@Override
	public String toString() {
		return "FareType [id=" + id + ", ticketClass=" + ticketClass
				+ ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((ticketClass == null) ? 0 : ticketClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FareType other = (FareType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (ticketClass == null) {
			if (other.ticketClass != null)
				return false;
		} else if (!ticketClass.equals(other.ticketClass))
			return false;
		return true;
	}

	public TicketClass getTicketClass() {
		return ticketClass;
	}

	public void setTicketClass(TicketClass ticketClass) {
		this.ticketClass = ticketClass;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}
}
