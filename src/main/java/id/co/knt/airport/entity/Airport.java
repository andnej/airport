package id.co.knt.airport.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="AIRPORTS")
@NamedQueries({
	@NamedQuery(name="Airport.findAll", query="select a from Airport a")
})
public class Airport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Long id;
	@Column (name="NAME", unique=true)
	private String name;
	@Column (name="LOCATION")
	private String location;
	@Column (name="DETAILS")
	private String details;
	
	@OneToMany(mappedBy="departureAirport", cascade=CascadeType.ALL)
	private List<FlightSchedule> departureSchedules = new ArrayList<FlightSchedule>();
	@OneToMany(mappedBy="arrivalAirport", cascade=CascadeType.ALL)
	private List<FlightSchedule> arrivalSchedules = new ArrayList<FlightSchedule>();
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public List<FlightSchedule> getDepartureSchedules() {
		return departureSchedules;
	}

	public void addDepartureFlightSchedule(FlightSchedule fs) {
		if (fs != null) {
			fs.setDepartureAirport(this);
			this.departureSchedules.add(fs);
		}
	}
	
	public void removeDepartureFlightSchedule(FlightSchedule fs) {
		if (fs != null) {
			this.departureSchedules.remove(fs);
			fs.setDepartureAirport(null);
		}
	}
	
	public List<FlightSchedule> getArrivalSchedules() {
		return arrivalSchedules;
	}

	public void addArrivalFlightSchedule(FlightSchedule fs) {
		if (fs != null) {
			fs.setArrivalAirport(this);
			this.arrivalSchedules.add(fs);
		}
	}
	
	public void removeArrivalFlightSchedule(FlightSchedule fs) {
		if (fs != null) {
			this.arrivalSchedules.remove(fs);
			fs.setArrivalAirport(null);
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airport other = (Airport) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Airport [id=" + id + ", name=" + name + ", location="
				+ location + "]";
	}
	
}
