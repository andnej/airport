package id.co.knt.airport.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "FARES")
@NamedQueries({ @NamedQuery(name = "Fare.findAll", query = "select f from Fare f") })
public class Fare implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "FARE_TYPE_ID")
	private FareType fareType;

	@Column(name = "ONE_WAY_FARE")
	private BigDecimal oneWayFare = BigDecimal.ZERO;
	@Column(name = "RETURN_FARE")
	private BigDecimal returnFare = BigDecimal.ZERO;

	@ManyToOne
	@JoinColumn(name = "flight_shedule_id")
	private FlightSchedule flightSchedule;
	
	@OneToMany(mappedBy="fare", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Booking> bookings = new ArrayList<Booking>();

	public Fare() {
		super();
	}

	public Long getId() {
		return id;
	}

	public FareType getFareType() {
		return fareType;
	}

	public void setFareType(FareType fareType) {
		this.fareType = fareType;
	}

	public BigDecimal getOneWayFare() {
		return oneWayFare;
	}

	public void setOneWayFare(BigDecimal oneWayFare) {
		this.oneWayFare = oneWayFare;
	}

	public BigDecimal getReturnFare() {
		return returnFare;
	}

	public void setReturnFare(BigDecimal returnFare) {
		this.returnFare = returnFare;
	}

	public FlightSchedule getFlightSchedule() {
		return flightSchedule;
	}

	public void setFlightSchedule(FlightSchedule flightSchedule) {
		this.flightSchedule = flightSchedule;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

}
