package id.co.knt.airport.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TICKET_CLASSES")
@NamedQueries({
	@NamedQuery(name="TicketClass.findAll", query="select tc from TicketClass tc")
})
public class TicketClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7334001210471736687L;
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID")
	private Long id;
	@Column(name="CODE")
	private String code;
	@Column(name="DESCRIPTION")
	private String description;
	
	@OneToMany(mappedBy="ticketClass", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<FareType> faretypes = new ArrayList<FareType>();
	
	@Override
	public String toString() {
		return "TicketClass [id=" + id + ", code=" + code + ", description="
				+ description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketClass other = (TicketClass) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public List<FareType> getFaretypes() {
		return faretypes;
	}
}
