package id.co.knt.airport.datamodel;

import java.util.List;

import id.co.knt.airport.entity.FlightSchedule;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class FlightScheduleDataModel extends ListDataModel<FlightSchedule> implements SelectableDataModel<FlightSchedule>{

	public FlightScheduleDataModel(){}
	public FlightScheduleDataModel(List<FlightSchedule> data){
		super(data);
	}
	
	@Override
	public Object getRowKey(FlightSchedule object) {
		// TODO Auto-generated method stub
		return object.getFlightNumber();
	}

	@Override
	public FlightSchedule getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data
        
        List<FlightSchedule> flightSchedules = (List<FlightSchedule>) getWrappedData();
        
        for(FlightSchedule flightSchedule : flightSchedules) {
            if(flightSchedule.getFlightNumber().equals(rowKey))
                return flightSchedule;
        }
        
        return null;
	}
        

}
