package id.co.knt.airport.datamodel;

import java.util.List;

import id.co.knt.airport.entity.FlightFare;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class FlightFareDataModel extends ListDataModel<FlightFare> implements SelectableDataModel<FlightFare>{

	public FlightFareDataModel(){}
	public FlightFareDataModel(List<FlightFare> data){
		super(data);
	}
	
	@Override
	public Object getRowKey(FlightFare object) {
		// TODO Auto-generated method stub
		return object.getModel();
	}

	@Override
	public FlightFare getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data
        
        List<FlightFare> flightFares = (List<FlightFare>) getWrappedData();
        
        for(FlightFare flightfare : flightFares) {
            if(flightfare.getModel().equals(rowKey))
                return flightfare;
        }
        
        return null;
	}
        

}
